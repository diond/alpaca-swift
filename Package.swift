// swift-tools-version:5.5
import PackageDescription

let package = Package(
	name: "Alpaca",
    products: [
        .library(
            name: "Alpaca",
            targets: ["Alpaca"]),
    ],
    dependencies: [
    ],
	targets: [
        .target(
            name: "Alpaca",
            dependencies: []),
		.testTarget(
            name: "Tests",
            dependencies: ["Alpaca"]),
	]
)
